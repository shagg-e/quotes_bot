const tmi = require("tmi.js");
const options = require(`./options.${process.env.NODE_ENV || 'local'}.js`); //Your options file
var mysql = require('mysql'); 

// connect to mysql
var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "unreal89",
  database: "quotes"
});

// Connect to twitch server
const client = new tmi.client(options);
client.connect();

// on chat message received
client.on("chat", (channel, userstate, message, self) => { 
   if(self) return; // dont listen to our own messages

   // check for signup command
   if(message == '!signup'){

    // prepare the insert query and run it
    var sql = "INSERT INTO channels (channel_name) SELECT '" + escape(userstate["display-name"]) + "' WHERE NOT EXISTS (SELECT channel_name FROM channels WHERE channel_name = '" + userstate["display-name"] + "');";
    con.query(sql, function (err, result) {
      if (err) throw err;
    });
    client.say(channel, 'You have been signed up and the bot will begin monitoring your channel in a few seconds!');
   }

   // check for stop command
   if(message =='!stop'){

    // prepare the insert query and run it
    var sql = "UPDATE channels SET active = 0, pending_action = 1 WHERE channel_name = '" + userstate["display-name"] + "';";
    con.query(sql, function (err, result) {
      if (err) throw err;
    });
    client.say(channel, 'You have stopped the bot from monitoring your channel, but all your quotes are still fine. Type "!start" to get it to start working again.');

    }

    // check for start command
    if(message =='!start'){

      // prepare the insert query and run it
      var sql = "UPDATE channels SET active = 1, pending_action = 1 WHERE channel_name = '" + userstate["display-name"] + "';";
      con.query(sql, function (err, result) {
        if (err) throw err;
      });
      client.say(channel, 'You have started the bot again, and it will once again monitor your channel in a few seconds!');
      }
   
  }
);