const tmi = require("tmi.js");
const options = require(`./options.${process.env.NODE_ENV || 'local'}.js`); //Your options file
const mysql = require('mysql'); 

// connect to mysql
const con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "unreal89",
  database: "quotes"
});

// Connect to twitch server
const client = new tmi.client(options);
client.connect();
// Initial connection to channels that should be active
// because in the case pm2 restarts for any reason we 
// will need to rejoin the active channels
client.on("connected", (address, port) => {
  let rawChannels = [];
  let targetChannels = [];
  let pending_action = 1;

  getJoinArray(pending_action, function(err, content){
    if (err){
      console.log(err);
    } else {
      rawChannels = content; // from db returns a list of objects with one property of channel_name, need to parse names out
    }

    rawChannels.forEach(function(element){
      targetChannels.push(element.channel_name.toLowerCase().replace("#", ""));
    });

    targetChannels.forEach(function(element){
      client.join(element);
      flipPending(element);
    });
  });
});



// loop for programmatically joining channels
client.on("connected", (address, port) => {

  let timer = setTimeout(joinChannels, 5000);
  function joinChannels() {

    // first create empty array that will become array of channels based on the channels
    // table in the database. we will then check that against already joined channels
    // with client.getChannels(), trim the overlap, then loop through the resulting
    // array to join the not-yet-joined channels
    let rawChannels = [];
    let targetChannels = [];
    let currentChannels = [];
    let rawLeaveChannels = [];
    let targetLeaveChannels = [];
    let pending_action = 1;

    getJoinArray(pending_action, function(err, content){

      if (err){
        console.log(err);
      } else {
        rawChannels = content; // from db returns a list of objects with one property of channel_name, need to parse names out

      }
      
      getLeaveArray(function(err, content){
        if (err){
          console.log(err);
        } else {
          rawLeaveChannels = content;
        }

        rawLeaveChannels.forEach(function(element){ // parse objects and push channel_name to clean/new array
          targetLeaveChannels.push(element.channel_name.toLowerCase().replace("#", ""));
        });

        targetLeaveChannels.forEach(function(element){
          client.part(element); // actually do the leaving of each in the array of channels to leave
          flipPending(element);
        });
      });

      rawChannels.forEach(function(element){
        targetChannels.push(element.channel_name.toLowerCase().replace("#", ""));
      });

      client.getChannels().forEach(function(element){ // using getChannels() to pull currently monitored channels from the tmijs client
        currentChannels.push(element.replace("#", ""));
      });

      targetChannels = targetChannels.filter(val => !currentChannels.includes(val)); // filter targetChannels based on currentChannels

      targetChannels.forEach(function(element){
        client.join(element); // actually join the channels now
        flipPending(element);

      });
    });

    setTimeout(joinChannels, 5000);

  }
});


// on chat message received
client.on("chat", (channel, userstate, message, self) => { 

  if(self) return; // dont listen to quotes_bot's own messages

    // deal with calling quotes by number and later random quotes outside of permissions check later on
    // since those are the only two things anyone else should be checked for other than mods and broadcaster
    if(message.startsWith("!quote ") && !(message.startsWith("!quote add")) && !(message.startsWith("!quote cooldown")) && !(message.startsWith("!quote random")) && !(message.startsWith("!quote edit"))){  // check for !quote but dont proceed if !quote add

      let quotenum = parseInt(message.replace("!quote ", "")); // trim and convert to int
  
  
      if(isNaN(quotenum)){ // checks if they did something like `!quote i83` when trying to do `!quote 83` or just screw up the syntax in general
        client.say(channel, `To call a quote, use "!quote #"`);
      }
  
      else { // pull the quote and repeat it in chat
  
        getChannelID(channel, function(err, content){  // we will be calling channel id so we know which channel id's unique quotenums to use
     
          let channel_id =''
        
          if (err){
                console.log(err);
              } else {
                channel_id = content;
              }
          
          getQuoteText(channel_id, quotenum, function(err, content){
            let quote_text = ''
            if (err){
              console.log(err);
            } else {
              quote_text = content;
            }
          
          canCallQuote(channel, function(err, content){
  
            let canCallQuote = true;
            if (err){
              console.log(err);
            } else {
              canCallQuote = content;
            }
  
  
            if (canCallQuote){
              client.say(channel, quote_text);
              saveLastTime(channel);
            }
          });
          });  
          });
      }
      }

    // random quote
    if(message == "!quote random"){
      getChannelID(channel, function(err, content){  // we will be calling channel id so we know which channel id's unique quotenums to use
    
          let channel_id ='';
        
          if (err){
                console.log(err);
              } else {
                channel_id = content;
              }
          
          let quote = '';

          randomQuote(channel_id, function(err, content){
            if (err){
              console.log(err);
            } else {
              quote = content;
            }

            canCallQuote(channel, function(err, content){
  
              let canCallQuote = true;
              if (err){
                console.log(err);
              } else {
                canCallQuote = content;
              }
    
    
              if (canCallQuote){
                client.say(channel, quote);
                saveLastTime(channel);
              }
            });
          });
        })}
  
  // all other functions contained within this if/elseif for permissions check
  if (userstate["mod"] === true || userstate["badges"]["broadcaster"] == 1){
    
    // deal with adding quotes
    if(message.startsWith("!quote add")){

      // first check if user shouldn't have permissions...not a mod or the streamer themselves
      if (userstate["mod"] === false && userstate["badges"]["broadcaster"] == 0){
        client.say(channel, `Only ` + channel.toString().replace("#", "") + ` and mods can add quotes.`);
      }
      // then do stuff
      else if (userstate["mod"] === true || userstate["badges"]["broadcaster"] == 1){
        addQuote(message.replace("!quote add ", ""), channel);
        getChannelID(channel, function(err, content){
          let channel_id = ''
      
          if (err){
              console.log(err);
            } else {
              channel_id = content;
            }

          getQuoteNum(channel_id, function(err, content){
            let quote_number = ''
            if (err){
              console.log(err);
            } else {
              quote_number = content;
            }

            client.say(channel, 'Quote #' + quote_number + ' was successfully added!'); 
          });   
            
        });
      }
    }  
    
    // allow mods/broadcaster to edit cooldown
      if(message.startsWith("!quote cooldown")){

        let cdnum = parseInt(message.replace("!quote cooldown ", ""));
        if(isNaN(cdnum)){
          client.say(channel, `To change the cooldown on quotes, a mod or broadcaster must type '!quote cooldown <number>'.`);
        } else if(cdnum < 1){
          client.say(channel, `The minimum cooldown on quotes is 1 second.`);
        } else {

          // need to add getChannelID --> changeCooldown logic
          getChannelID(channel, function(err, content){  // we will be calling channel id so we know which channel id's unique quotenums to use
      
          let channel_id =''
        
          if (err){
                console.log(err);
              } else {
                channel_id = content;
              }
  
          //editQuote(channel_id, quotenum, message.replace(`!quote edit ${quotenum}`, ``)); //send channel id, quotenum itself, and the message trimmed down to the message itself
          //client.say(channel, `Successfully edited quote #${quotenum}`);
          changeCooldown(channel_id, cdnum);

          });
        }

      }

    // deal with editing quotes  
      else if(message.startsWith("!quote edit")){



        if(isNaN(quotenum)){ // checks if they did something like `!quote i83` when trying to do `!quote 83` or just screw up the syntax in general
          client.say(channel, `To edit a quote, use "!quote edit #"`);
        }

        else { 

          getChannelID(channel, function(err, content){  // we will be calling channel id so we know which channel id's unique quotenums to use
      
            let channel_id =''
          
            if (err){
                  console.log(err);
                } else {
                  channel_id = content;
                }
    
            editQuote(channel_id, quotenum, message.replace(`!quote edit ${quotenum}`, ``)); //send channel id, quotenum itself, and the message trimmed down to the message itself
            client.say(channel, `Successfully edited quote #${quotenum}`);

            });

    
        }

        }
  }  
  
    
    
    
  }
);

// add the quote
function addQuote(quote, channel, callback) {

  

  // get channel id
  getChannelID(channel, function(err, content){
   
    let channel_id =''

    if (err){
          console.log(err);
        } else {
          channel_id = content;
        }
    
    // prepare and execute the necessary subquery
    con.query(`SET @sub = (SELECT MAX(quote_number)+1 FROM quotes WHERE channel_id = ` + channel_id + `);`, [channel_id], function (err, result) {
    if (err) throw err;
    });

    con.query(`INSERT INTO quotes (channel_id, quote_text, quote_number) VALUES (?, ?, coalesce(@sub, 1));`, [channel_id, con.escape(quote)], function (err, result) {
      if (err) throw err;
      console.log("Quote added for " + channel);
    });

  });



  
  
}


// get channel_id
function getChannelID(channel, callback){

  // run it
  con.query(`SELECT channel_id FROM channels WHERE channel_name = ?;`, [channel.toString().replace("#", "")], function (err, result) {
    if (err) {
      callback(err, null);
    } else 
      callback(null, result[0].channel_id);
  });
}

// return random quote per channel id
function randomQuote(channel_id, callback){
  
    con.query(`SELECT quote_text FROM quotes WHERE channel_id = ? ORDER BY RAND() LIMIT 1;`, [channel_id],function (err, result) {
      if (err) {
        callback(err, null);
      } else 
        callback(null, result[0].quote_text);
    });
}

// edit quote 
function editQuote(channel_id, quotenum, quote, callback) {

   // run query
  con.query(`UPDATE quotes SET quote_text = ? WHERE channel_id = ? AND quote_number = ?;`, [quote, channel_id, quotenum], function (err, result) {
    if (err) throw err;
  });
}

// edit cooldown
function changeCooldown(channel_id, cdnum, callback){
  // run query
  con.query(`UPDATE channels SET timer_setting = ? WHERE channel_id = ?;`, [cdnum, channel_id], function (err, result){
    if (err) throw err;
  });
}

// get quote text
function getQuoteText(channel_id, quotenum, callback){
  
  con.query(`SELECT quote_text FROM quotes WHERE channel_id = ? AND quote_number = ?;`, [channel_id, quotenum],function (err, result) {
    if (err) {
      callback(err, null);
    } else 
      callback(null, result[0].quote_text);
  });

}

// get array of channels to monitor
function getJoinArray(pending_action, callback){

  // run it
  con.query(`SELECT channel_name FROM channels WHERE channel_name NOT IN ('quotes_bot') AND active = 1;`, function (err, result) {
    if (err) {
      callback(err, null);
    } else 
      callback(null, result);
  });

}

// get array of channels to leave
function getLeaveArray(callback){

    // run it
    con.query(`SELECT channel_name FROM channels WHERE active = 0 AND pending_action = 1;`, function (err, result) {
      if (err) {
        callback(err, null);
      } else 
        callback(null, result);
    });
}

function flipPending(channel_name){

  // prepare the query and run it
  var sql = "UPDATE channels SET pending_action = 0 WHERE channel_name = ?";

  con.query(`UPDATE channels SET pending_action = 0 WHERE channel_name = ?`, [channel_name], function (err, result) {
    if (err) throw err;
  });
}

function getQuoteNum(channel_id, callback){

    // run it
    con.query(`SELECT MAX(quote_number) FROM quotes WHERE channel_id = ?;`, [channel_id], function (err, result) {
      if (err) {
        callback(err, null);
      } else 
        callback(null, result[0]['MAX(quote_number)']);
    });

  };

  
function saveLastTime(channel){

  let d = new Date();
  let curTime = d.getTime () / 1000;

    con.query(`UPDATE channels SET last_time = ? WHERE channel_name = ?;`, [curTime, channel.toString().replace("#", "")], function (err, result) {
      if (err) throw err;
    });

};  

function canCallQuote(channel, callback){

  getTimerSetting(channel, function(err, content){

    let timer_setting;
    if (err){
      console.log(err);
    } else {
      timer_setting = content;
    }
    
    getLastTime(channel, timer_setting, function(err, content){

      let last_time;
      if (err){
        console.log(err);
      } else {
        last_time = content;
      }

      let b = new Date();
      let curTime = b.getTime () / 1000;

      if((curTime - last_time) > timer_setting){
        callback(null, true);
      } else {
        callback(null, false);
      }

    });
  
  });  

};

function getTimerSetting(channel, callback){

      // run it
      con.query(`SELECT timer_setting FROM channels WHERE channel_name = ?;`, [channel.toString().replace("#", "")], function (err, result) {
        if (err) {
          callback(err, null);
        } else 
          callback(null, result[0].timer_setting);
      });
};

function getLastTime(channel, timer_setting, callback){

  // run it
  con.query(`SELECT last_time FROM channels WHERE channel_name = ?;`, [channel.toString().replace("#", "")], function (err, result) {
    if (err) {
      callback(err, null);
    } else 
      callback(null, result[0].last_time);
  });
};
